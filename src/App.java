import com.devcamp.models.Account;
import com.devcamp.models.Customer;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Tan Loc",20);
        System.out.println("Customer 1:");
        System.out.println(customer1);
        System.out.println("------------------------------------");

        Customer customer2 = new Customer(2, "My Cam",30);
        System.out.println("Customer 2:");
        System.out.println(customer2);
        System.out.println("------------------------------------");

        Account account1 = new Account(1, customer1, 4500.0);
        System.out.println("Account 1:");
        System.out.println(account1);
        System.out.println("------------------------------------");

        Account account2 = new Account(1, customer2, 5500.0);
        System.out.println("Account 2:");
        System.out.println(account2);
        System.out.println("------------------------------------");
    }
}
